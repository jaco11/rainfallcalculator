// total_2016
$(document).ready(function(){

    // Calculator for 2016
    $('.calc_2016').change(function(){
        let total = 0;
        $('.calc_2016').each(function(){
            if($(this).val() != '')
            {
                total += parseInt($(this).val());
            }
        });
        $('#total_2016').html(total);
    });

    // Calculator for 2017
    $('.calc_2017').change(function(){
        let total = 0;
        $('.calc_2017').each(function(){
            if($(this).val() != '')
            {
                total += parseInt($(this).val());
            }
        });
        $('#total_2017').html(total);
    });

    // Calculator for 2018
    $('.calc_2018').change(function(){
        let total = 0;
        $('.calc_2018').each(function(){
            if($(this).val() != '')
            {
                total += parseInt($(this).val());
            }
        });
        $('#total_2018').html(total);
    });

    // Calculator for 2019
    $('.calc_2019').change(function(){
        let total = 0;
        $('.calc_2019').each(function(){
            if($(this).val() != '')
            {
                total += parseInt($(this).val());
            }
        });
        $('#total_2019').html(total);
    });

    // Calculator for 2020
    $('.calc_2020').change(function(){
        let total = 0;
        $('.calc_2020').each(function(){
            if($(this).val() != '')
            {
                total += parseInt($(this).val());
            }
        });
        $('#total_2020').html(total);
    });

    // Calculator for 2021
    $('.calc_2021').change(function(){
        let total = 0;
        $('.calc_2021').each(function(){
            if($(this).val() != '')
            {
                total += parseInt($(this).val());
            }
        });
        $('#total_2021').html(total);
    });


})(jQuery);

